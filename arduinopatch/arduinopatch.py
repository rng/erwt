#!/usr/bin/env python

import sys
import os
import shutil
from subprocess import check_call

def extract(sourcefile):
	_, ext = os.path.splitext(sourcefile)
	if ext == ".tgz":
		check_call(["tar", "xzf", sourcefile])
	elif ext == ".zip":
		check_call(["unzip", "-q", sourcefile])

def compress(sourcedir, resultname):
	if resultname.endswith("tgz"):
		check_call(["tar", "czf", resultname, sourcedir])
	elif resultname.endswith("zip"):
		check_call(["zip", "-rq", resultname, sourcedir])

sourcefile = sys.argv[1]

source, ext = os.path.splitext(sourcefile)
source = source.split("-")
dirname = "-".join(source[:2])
newdirname = "arduino-erwt-%s" % (source[1])

extract(sourcefile)

if not os.path.exists(dirname):
	raise Exception("can find extrated dir: %s" % dirname)
shutil.copytree("../firmware/arduino/erwt", "%s/hardware/erwt" % dirname)
shutil.copytree("../firmware/arduino/examples/TestAll", "%s/examples/TestAll" % dirname)
os.rename(dirname, newdirname)
compressname = "%s-%s%s" % (newdirname, source[2], ext)
compress(newdirname, compressname)
check_call(["rm", "-rf", newdirname])
