GERB_ALL = $(BRD:.brd=.all)
GERB_VIEW = $(BRD:.brd=.view)
GERB_ZIP = $(BRD:.brd=.zip)
SCH = $(BRD:.brd=.sch)

BOM_SCR = $(realpath bom-json.ulp)

all: gerber bom

gerber: $(GERB_ALL)
view: $(GERB_VIEW)
zip: $(GERB_ZIP)
bom: $(BRD:.brd=.json)

%.zip: %.xln %.bor %.plc %.stc %.cmp %.sts %.pls %.sol
	zip $@ $^

%.view: %.xln %.bor %.plc %.stc %.cmp %.sts %.pls %.sol
	gerbv $^

%.json: %.sch
	@echo "[BOM] $<"
	@eagle -C'edit $<; run $(BOM_SCR); quit;' $<
	@mv bom.json $@

### GERBER GENERATION

.PRECIOUS : %.cmp %.sol %.stc %.sts %.plc %.pls %.bor %.xln

%.all: %.cmp %.sol %.stc %.sts %.plc %.pls %.xln %.bor 
	touch $@

# Component side
%.cmp : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< Top Pads Vias
# Solder side
%.sol : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< Bottom Pads Vias
# Solder stop mask comp. side
%.stc : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< tStop
# Solder stop mask sold. side
%.sts : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< bStop
# Silkscreen comp. side
%.plc : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< Dimension tPlace tNames
# Silkscreen solder. side
%.pls : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< Dimension bPlace bNames
# Drill data for NC drill st.
%.xln : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d EXCELLON -E 0.05 -R $(DRILLFILE) -o $@ $< Drills Holes
# Outline
%.bor : %.brd
	@echo "[CAM] $@"
	@eagle -X -N -d GERBER_RS274X -o $@ $< Dimension

clean:
	@echo "[CLEAN]"
	@rm -rf *.cmp *.sol *.stc *.sts *.plc *.pls *.xln *.bor *.dri *.gpi *.all \
		     *.b\#* *.s\#* *.blend1 *.json
