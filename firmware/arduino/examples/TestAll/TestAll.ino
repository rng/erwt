void setup() {
}

void loop() {
  forward(1);
  led(LED_LEFT, 1);
  led(LED_RIGHT, 0);
  delay(1000);
  reverse(1);
  led(LED_LEFT, 0);
  led(LED_RIGHT, 1);
  delay(1000);
}
