#include "Erwt.h"
#include "Arduino.h"

#define LINE_LIMIT 100

void motor(uint8_t which, int16_t velocity) {
  if (which == MOTOR_RIGHT) {
    if (velocity > 0) {
      PORTD |= _BV(5);
      PORTD &= ~_BV(6);
    } else if (velocity < 0) {
      PORTD |= _BV(6);
      PORTD &= ~_BV(5);
    } else {
      PORTD &= ~_BV(6);
      PORTD &= ~_BV(5);
    }  
  } else {
    if (velocity > 0) {
      PORTB |= _BV(3);
      PORTD &= ~_BV(3);
    } else if (velocity < 0) {
      PORTD |= _BV(3);
      PORTB &= ~_BV(3);
    } else {
      PORTB &= ~_BV(3);
      PORTD &= ~_BV(3);
    }  
  }
}

void forward(int16_t velocity) {
	motor(MOTOR_LEFT, velocity);
	motor(MOTOR_RIGHT, velocity);
}

void reverse(int16_t velocity) {
	motor(MOTOR_LEFT, -velocity);
	motor(MOTOR_RIGHT, -velocity);
}

void led(uint8_t which, int on) {
  if (on)
    PORTB |= _BV(which);
  else
    PORTB &= ~_BV(which);
}

int button() {
  return (PIND & _BV(2))==0
  ;
}

uint16_t light(uint8_t which) {
  return 1023 - analogRead(which);
}

void line_power(int enable) {
  if (enable)
    PORTB |= _BV(2);
  else
    PORTB &= ~_BV(2);
}

uint8_t line() {
  line_power(1);
  delay(1);
  int lines = 0;
  if (analogRead(1) > LINE_LIMIT) lines |= 1; 
  if (analogRead(2) > LINE_LIMIT) lines |= 2; 
  if (analogRead(3) > LINE_LIMIT) lines |= 4; 
  line_power(0);
  return lines;
}

int vbatt() {
  int v = analogRead(0);
  return v;
}

void erwt_ioinit() {
	DDRB |= _BV(1)|_BV(2)|_BV(3)|_BV(6)|_BV(7);
	DDRD |= _BV(3)|_BV(5)|_BV(6);
	PORTD |= _BV(2);

	motor(MOTOR_LEFT, 0);
	motor(MOTOR_RIGHT, 0);
}
