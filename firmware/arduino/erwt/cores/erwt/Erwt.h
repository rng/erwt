#ifndef Erwt_h
#define Erwt_h

#include <stdint.h>

#define LED_LEFT 6
#define LED_RIGHT 7

#define LIGHT_LEFT 7
#define LIGHT_RIGHT 6

#define MOTOR_RIGHT 0
#define MOTOR_LEFT 1

void motor(uint8_t which, int16_t velocity);
void forward(int16_t velocity);
void reverse(int16_t velocity);
void led(uint8_t which, int on);
int button();
uint16_t light(uint8_t which);
uint8_t line();
int vbatt();
void erwt_ioinit();

#endif
