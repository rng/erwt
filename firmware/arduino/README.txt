Basic Arduino core files for Erwt.

$ hg clone https://bitbucket.org/rng/erwt
$ cd arduino/hardware
$ ln -s ~/erwt/firmware/arduino/erwt erwt
$ cd ../../examples
$ ln -s ~/erwt/firmware/arduino/examples Erwt
